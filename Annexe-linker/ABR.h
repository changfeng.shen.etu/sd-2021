#if ! defined (ABR_H)
#define ABR_H 1

#include "symbole.h"

/**
 * @file 
 * @brief Les types nécessaires à l'implantation des arbres binaires de
 * recherche (ABR) et aux piles d'arbres binaires de recherche. 
 * Les valeurs portées par les noeuds des arbres sont des symboles.
 * @author F. Boulier
 * @date novembre 2010
 */

/***********************************************************************
 * IMPLANTATION
 ***********************************************************************/

/**
 * @struct ABR
 * @brief Le type \p struct \p ABR permet de manipuler des 
 * arbres binaires
 * de recherche. Le champ \p value contient l'adresse d'un symbole.
 * Le champ \p gauche contient un sous-arbre
 * dont les symboles sont tous plus petits, lexicographiquement que
 * \p value. Le champ \p droit contient un sous-arbre dont les
 * symboles sont tous plus grands que \p value. Lorsque la variable
 * \p AVL du préprocesseur est définie, la structure comporte deux champs
 * supplémentaires : le champ \p hauteur_gauche, qui contient la hauteur
 * du sous-arbre gauche et le champ \p hauteur_droit, qui contient la
 * hauteur du sous-arbre droit.
 */

struct ABR
{   struct ABR* gauche;     /**< symboles plus petits */
    struct ABR* droit;      /**< symboles plus grands */
    struct symbole value;   /**< symbole courant */
#if defined (AVL)
    int hauteur_gauche;     /**< la hauteur du sous-arbre gauche */
    int hauteur_droit;      /**< la hauteur du sous-arbre droit */
#endif
};

#define NIL (struct ABR*)0

/**
 * @struct pile_ABR
 * @brief Le type \p struct \p pile_ABR fournit une 
 * implantation élémentaire d'une pile d'arbres binaires de recherche.
 * Le champ \p tab pointe vers une zone où sont stockés des adresses
 * d'arbres binaires de recherche. 
 * Le champ \p alloc contient le nombre d'emplacements alloués à \p tab.
 * Le champ \p sp contient l'indice de l'élément en sommet de pile.
 * Le champ \p tab contient les éléments.
 * Quand la pile est vide, \p sp vaut \f$-1\f$.
 */

struct pile_ABR
{   int sp;                          /**< l'indice du sommet de pile */
    int alloc;                       /**< nombre d'emplacements alloués */
    struct ABR** tab;                /**< pointe vers la zone de stockage */
};

/***********************************************************************
 * PROTOTYPES DES FONCTIONS (TYPE ABSTRAIT)
 ***********************************************************************/

extern void init_ABR (struct ABR*);
extern void clear_ABR (struct ABR*);
extern struct ABR* new_ABR (void);
extern int hauteur_ABR (struct ABR*);
extern void imprimer_ABR (struct ABR*);
extern void dot_imprimer_ABR (char*, struct ABR*);

extern int rechercher_symbole_dans_ABR (struct symbole**, char*, struct ABR*);
extern struct ABR* ajouter_symbole_dans_ABR (struct ABR*, struct symbole*);

extern void init_pile_ABR (struct pile_ABR*);
extern void vider_pile_ABR (struct pile_ABR*);
extern void clear_pile_ABR (struct pile_ABR*);
extern bool est_pleine_pile_ABR (struct pile_ABR*);
extern bool est_vide_pile_ABR (struct pile_ABR*);
extern void empiler_ABR (struct pile_ABR*, struct ABR*);
extern void depiler_ABR (struct ABR**, struct pile_ABR*);

#endif
